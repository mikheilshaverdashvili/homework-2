package com.example.homework2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()
    }

    private fun init() {
        val userModel = intent!!.getParcelableExtra("userModel") as UserModel


        nameEdit.hint = userModel.name
        lastNameEdit.hint = userModel.lastName
        emailEdit.hint = userModel.email
        yearOfBirthEdit.hint = userModel.yearOfBirth.toString()
        genderEdit.hint = userModel.gender

        saveButton.setOnClickListener{
            saveInfo()
        }

        cancelButton.setOnClickListener{
            cancelInfo()
        }
    }

    private fun cancelInfo(){
        val intent = Intent(this, MainActivity::class.java)
    }

    private fun saveInfo(){
        val intent = intent
        intent.putExtra("name", nameEdit.hint.toString())
        intent.putExtra("lastName", lastNameEdit.hint.toString())
        intent.putExtra("email", emailEdit.hint.toString())
        intent.putExtra("yearOfBirth", yearOfBirthEdit.hint.toString().toInt())
        intent.putExtra("gender", genderEdit.hint.toString())
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

}
