package com.example.homework2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val REQUEST_CODE = 1337

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
    openSecondActivity.setOnClickListener(){
        openSecondActivity()
        }
    }

    private fun openSecondActivity(){
        val userModel = UserModel(nameView.text.toString(), lastNameView.text.toString(), emailView.text.toString(), yearOfBirthView.text.toString().toInt(), genderView.text.toString())

        val intent = Intent(this, SecondActivity::class.java)
        intent.putExtra("userModel", userModel)
        startActivityForResult(intent, REQUEST_CODE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE){
            val name = data!!.extras!!.getString("name", "")
            val lastName = data!!.extras!!.getString("lastName", "")
            val email = data!!.extras!!.getString("email", "")
            val yearOfBirth = data!!.extras!!.getString("yearOfBirth", "").toInt()
            val gender = data!!.extras!!.getString("gender", "")
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}
